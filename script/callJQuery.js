$(document).ready(function () {
    $(window).on("scroll", () => {
        if ($(window).scrollTop()) {
            $("#navbar").addClass("sticky");
        }
        else {
            $("#navbar").removeClass("sticky");
        }
    });

    $(".navbar-toggler").click(function () {
        $(this).toggleClass("navbar-toggler-bg")
    });

    $("#owl-example-1").owlCarousel({
        items: 1,
        loop: true,
        center: true,
        nav: true,
        dots: false,
        autoplay: true,
        autoplayTimeout: 9000,
        smartSpeed: 2000,
    });

    $("#owl-example-2").owlCarousel({
        margin: 20,
        items: 1,
        loop: true,
        nav: true,
        dots: false,
        autoplay: false,
        autoplayTimeout: 6000,
        smartSpeed: 1500,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            1170: {
                items: 3
            }
        }
    });

    $("#owl-example-3").owlCarousel({
        margin: 20,
        items: 1,
        loop: false,
        nav: true,
        dots: false,
        autoplay: false,
        autoplayTimeout: 6000,
        smartSpeed: 1500,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            1170: {
                items: 3
            }
        }
    });
});

jQuery(function ($) {

    'use strict';

    (function () {

        $('body').append('<div id="toTop" class="top"><i class="fa fa-angle-up"></i></div>');

        $(window).on("scroll", function (e) {
            if ($(this).scrollTop() != 0) {
                $('#toTop').fadeIn();
            } else {
                $('#toTop').fadeOut();
            }
        });

        $('#toTop').on('click', function () {
            $("html, body").animate({ scrollTop: 0 }, 600);
            return false;
        });

    }());
});