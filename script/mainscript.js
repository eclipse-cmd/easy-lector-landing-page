var acc = document.getElementsByClassName("accordion");
let i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function () {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
        } else {
            panel.style.maxHeight = panel.scrollHeight + "px";
        }
    });
}

const hamburger = document.querySelector('#hamburger');
hamburger.addEventListener('click', function () {
    hamburger.classList.toggle('close-hamburger')
});

(function pageLoader() {
    const preloader = document.getElementById('preloader');
    const main_page = document.getElementById('mainContent');
    const top = document.getElementById('toTop');

    setTimeout(function () {
        preloader.style.display = 'none';
        main_page.style.display = 'block';
    }, 5000)
})();

(function createDiv() {
    const main_page = document.getElementById('mainContent');
    const myDiv = document.createElement('div')
    const top = document.getElementById('toTop');

    myDiv.className = 'top';
    myDiv.id = 'toTop';
    myDiv.innerHTML = `<i class="fa fa-angle-up"></i>`
    main_page.appendChild(myDiv);

    console.log(myDiv)

    top.addEventListener('click', function () {
        window.scrollTo(0, 0);
    })
});